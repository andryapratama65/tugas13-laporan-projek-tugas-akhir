<html>
    <head>
        <title>Input Data</title>
        <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <header>
<h1>Form Data Mahasiswa Universitas Ahmad Dahlan Fakultas Teknologi Industri</h1>
    </header>   
<main>
    <article>
<div class="kotak_login">
<p class="tulisan_login"><b>Masukkan Form</b></p>
<Form name="formmahasiswa" action="output.php" method="POST" onSubmit="validasi()">
<div>
    <label>Nama</label> <br>
    <Input type="text" name="nama" id="nama" class="form_login">
</div>
<div>
    <label>Nim</label> <br>
    <Input type="text" name="nim" id="nim" class="form_login">         
</div>
<div>
    <label>Email</label> <br>
    <Input type="email" name="emil" class="form_login">
</div>
<div>
    <label>Usia</label> <br>
    <Input type="number" name="usia" class="form_login">
</div>
<div>    
    <label>Tanggal Lahir</label> <br>
    <Input type="date" name="tanggal_lahir" id="ttl" class="form_login">
</div>
<div>
    <label>Alamat</label> <br>
    <textarea name="alamat" id="alamat" class="form_login"></textarea>
</div>
<div>
    <label>Telepon</label> <br>
    <Input type="text" name="telepon" id="telepon" class="form_login">
</div>
<div style="margin-bottom: 1rem;">
    <label>Jenis kelamin</label> <br>
    <input type="radio" name="jenis_kelamin" value="laki-laki" id="jk"> laki-laki <br>
    <input type="radio" name="jenis_kelamin" value="perempuan" id="jk"> perempuan
</div> 
<div style="margin-bottom: 1rem;">
    <label>Agama</label> <br>
    <select name="agama" class="form_login">
    <option value="Islam" id="agm"> Islam </option>
    <option value="kristen" id="agm"> kristen </option>       
    <option value="kaolik" id="agm"> katolik </option>
    <option value="hindu" id="agm"> hindu </option>
    <option value="buddha" id="agm"> buddha </option>
    <option value="kong hu cu" id="agm"> kong hu cu </option>
    </select>
</div> 
<div style="margin-bottom: 1rem;">
    <label>Status</label> <br>
    <select name="status" class="form_login">
        <option value="kawin" id="sts">kawin</option>
        <option value="belum kawin" id="sts">Belum Kawin</option>
    </select>
<div>
<div style="margin-bottom: 1rem;">
    <label>Program Studi</label> <br>
    <input type="radio" name="prodi" value="Teknik Informatika" id="jurusan"> Teknik Informatika <br>
    <input type="radio" name="prodi" value="Teknik Industri" id="jurusan"> Teknik Industri <br>            
    <input type="radio" name="prodi" value="Teknik Elektro" id="jurusan"> Teknik Elektro <br>
    <input type="radio" name="prodi" value="Teknik Kimia" id="jurusan"> Teknik Kimia <br>
    <input type="radio" name="prodi" value="Teknologi pangan" id="jurusan"> Teknologi pangan <br>
</div>
<div style="margin-bottom: 1ren;">
    <label>hobi</label> <br>
    <input type="checkbox" name="hobi[]" value="berenang" id="hobi"> Berenang <br>
    <input type="checkbox" name="hobi[]" value="sepak bola" id="hobi"> Sepak Bola <br>
    <input type="checkbox" name="hobi[]" value="bulu tangkis" id="hobi"> Bulu Tangkis <br>
    <input type="checkbox" name="hobi[]" value="ngoding" id="hobi"> Ngoding <br>
    <input type="checkbox" name="hobi[]" value="main game" id="hobi"> Main game <br>
    <input type="checkbox" name="hobi[]" value="bermain alat musik" id="hobi"> Bermain Alat Musik <br>  
</div>
<div style="margin-bottom: 1rem;">
    <label>Prestasi Yang Pernah Diraih</label> <br>
    <textarea name="prestasi" class="form_login" id="goal"></textarea>
</div>
<div>
    <input type="submit" class="tombol_login" value="submit">
</div>
</form>
</article>
<aside>
<div class="kotak_login2">
    <p class="tulisan_login"><b>Masukkan Form</b></p>
    <form name="formmahasiswa2" action="output2.php" method="POST" onSubmit="validasi2()" name="input">
    <div>
        <label>Nilai Uji Kompetensi Pemograman Web</label> <br>
        <input type="text" name="nilai" id="nilai" class="form_login">
    </div>
    <div>
        <label>Masukkan tinggi segitiga</label>
        <input type="text" name="tinggi" id="tinggi" class="form_login">
    </div>
    <div>
        <label>Masukkan jari-jari lingkaran</label>
        <input type="text" name="jari" id="jari" class="form_login">
    </div>
    <div>
        <label>Masukkan Mata Kuliah</label>
        <textarea name="matkul" class="form_login" id="matkul"></textarea>
    </div>
    <div>
        <label>Masukkan Makanan Favorit</label>
        <textarea name="makfov" class="form_login" id="makfov"></textarea>
    </div>
    <div>
        <input type="submit" name="input" class="tombol_login" value="submit">
    </div>
    </form>
</aside>
</main>
    <div>
        <footer>
            Copyright &copy Bintang Andrya Pratama ||2200018290
        <footer>
    </div>
    <script type="text/javascript">
    function validasi() {
        if (document.forms["formmahasiswa"]["nama"].value == ""){
                alert("Nama Tidak Boleh Kosing");
                document.forms["formmahasiswa"]["nama"].focus();
                return false;
        }
        if (document.forms["formmahasiswa"]["nim"].value == "") {
                alert("NIM Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["nim"].focus();
                return false;
        }
        if (document.forms["formmahasiswa"]["email"].value == "") {
                alert("Email Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["email"].focus();
                return false;
        }
        if (document.forms["formmahasiswa"]["usia"].value == "") {
                alert("Usia Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["usia"].focus();
                return false;
        }
        if (document.forms["formmahasiswa"]["ttl"].value == "") {
                alert("Tanggal lahir Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["ttl"].focus();
                return false;
        }
        if (document.forms["formmahasiswa"]["alamat"].value == "") {
                alert("Alamat Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["alamat"].focus();
                return false;    
        }
        if (document.forms["formmahasiswa"]["telepon"].value == "") {
                alert("Telepon Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["telepon"].focus();
                return false;
        }
        if (document.forms["formmahasiswa"]["jenis_kelamin"].value == "") {
                alert("Jenis_kelamin Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["jenis_kelamin"].focus();
                return false;  
        }
        if (document.forms["formmahasiswa"]["agama"].value == "") {
                alert("Agama Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["agama"].focus();
                return false; 
        }
        if (document.forms["formmahasiswa"]["status"].value == "") {
                alert("Status Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["status"].focus();
                return false; 
        }
        if (document.forms["formmahasiswa"]["prodi"].value == "") {
                alert("Prodi Tidak Boleh Kosong");
                document.forms["formmahasiswa"]["prodi"].focus();
                return false;
        }                                     
        if (document.forms["formmahasiswa"]["hobi"].value == "") {
                alert("pilih Hobi");
                document.forms["formmahasiswa"]["hobi"].focus();
                return false;     
            }
    }
   </script>       
    <script type="text/javascript">
    function validasi2() {
        if (document.forms["formmahasiswa2"]["nilai"].value == ""){
                alert("Nilai Tidak Boleh Kosong");
                document.forms["formmahasiswa2"]["nilai"].focus();
                return false;
        }
        if (document.forms["formmahasiswa2"]["tinggi"].value == ""){
                alert("Tinggi Tidak Boleh Kosong");
                document.forms["formmahasiswa2"]["tinggi"].focus();
                return false;
        }
        if (document.forms["formmahasiswa2"]["jari"].value == ""){
                alert("Jari-jari Tidak Boleh Kosong");
                document.forms["formmahasiswa2"]["jari"].focus();
                return false;
        }
        if (document.forms["formmahasiswa2"]["matkul"].value == ""){
                alert("Matkul Tidak Boleh Kosong");
                document.forms["formmahasiswa2"]["matkul"].focus();
                return false;  
        }
        if (document.forms["formmahasiswa2"]["makfov"].value == ""){
                alert("Makanan favorit Tidak Boleh Kosong");
                document.forms["formmahasiswa2"]["makfov"].focus();
                return false;                    
            }
    } 
    </script>
    </body>
</html>



   
   